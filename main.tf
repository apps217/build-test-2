terraform {
  backend "http" {}
}

variable "aws_access_key" {}
variable "aws_secret_key" {}

provider "aws" {
  region     = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

module "instance" {
  source            = "git::https://gitlab.com/infra-gLo/AWS/modules/ec2-instance.git"
  ec2_instance_type = "t3.micro"
  ec2_instance_name = "Test"
  ec2_ami_id        = "ami-026b57f3c383c2eec"

}
